//
//  main.m
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 16..
//  Copyright noizze.net 2012년. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
    [pool release];
    return retVal;
}
