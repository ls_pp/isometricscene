//
//  AppDelegate.h
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 16..
//  Copyright noizze.net 2012년. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

@end
