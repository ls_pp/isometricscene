//
//  isoGame.h
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 16..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "isoCube.h"
#import "isoDice.h"
#import "isoPlayer.h"

@interface isoGame : NSObject
{
  int _mapZ[40][40]; // 0..39][0..39] 각 블록의 높이를 0 = 없음 1 부터로 표시 지표면 보다 낮은 곳은 ? 일단은 다른 변수를 사용 할 계획 
  BOOL _map[40][40][20];
  
  CCLayer *ThisLayer;
  
  float SCREEN_HEIGHT;
  float SCREEN_WIDTH;
  float CUBE_UNIT_WIDTH;
  float CUBE_UNIT_HEIGHT;
  
  NSMutableArray *cubes;
  NSMutableArray *ownedCubes;
  
  isoDice *_dice;
  int _PlayerNumber;
  NSMutableArray *players;
  
  isoCube *startCube;
  
  int GAMETURNS; // 전체 턴 카운트 
}

// init 
-(id)  initWithCCLayer:(CCLayer *)thisLayer standardZ:(int)zidx; 

// turn 관리
-(void) addTurnCount;
-(int) getTurnCount;

// 큐브 소유 관련 
-(void) setCube:(isoCube *)cube owner:(int)playerNumber;
-(void) collectBill:(int)playerNumber;

// 게임 구성 / map 관련 
-(void) putCubeOnMap_xx:(int)xx mapyy:(int)yy zz:(int)zz cubeType:(int)ctype;
-(void) setStartingCube:(isoCube *)_startCube;
-(isoCube *) getLastCube;
-(isoCube *) getFirstCube;
-(void) bondCubesLeft:(isoCube *)lCube Right:(isoCube *)rCube;
-(void) reportBrokenLinks;

// player 
-(void) setPlayerNumber:(int)num init:(BOOL)init;
-(void) movePlayer_no:(int)pn dice:(int)dice;
-(NSMutableArray *) getPlayers;

// dice 
-(void) drawDice;
-(int) stopDice;
-(void) removeDice;
-(BOOL) isRollDice;
// 내부 메소드 
//-(void) putCubeOnPosition_ccp:(CGPoint)point;
@end

