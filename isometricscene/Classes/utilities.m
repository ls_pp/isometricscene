//
//  utilities.m
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 17..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import "utilities.h"
/*
@implementation utilities

@end
*/

CGPoint _convert_map2ccp(int _xx, int _yy, int _zz)
{
  return CGPointMake((CUBE_UNIT_WIDTH/2 + (_xx * (CUBE_UNIT_WIDTH / 2)) + (_yy * (CUBE_UNIT_WIDTH / 2)) ), 
                     (SCREEN_HEIGHT/2 + (_xx * (CUBE_UNIT_HEIGHT / 2)) - (_yy * (CUBE_UNIT_HEIGHT / 2)) ));
}

