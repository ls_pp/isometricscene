//
//  isoDice.m
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 17..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import "isoDice.h"
#import <stdlib.h> // for random 

@implementation isoDice

-(id) initWithLayer:(CCLayer *)layer
{
  ThisLayer = layer;
  onRoll = NO;
  
  return self;
}

-(void) drawDice
{
  dicebg = [CCSprite spriteWithFile:@"dice_bg.png"];
  dicebg.position = ccp(350, 320); //ccp(300,250);
  dicebg.opacity = 160;
  [ThisLayer addChild:dicebg z:149];

  
  id appearance = [CCMoveTo actionWithDuration:0.25f position:ccp(350, 250)];
  [dicebg runAction:[CCSequence actions:
                     appearance, 
                     [CCCallFunc actionWithTarget:self selector:@selector(drawNumberAndAction)],
                     nil]];
}
   
-(void) drawNumberAndAction  
{
  di = [CCLabelTTF labelWithString:@"1"
                        dimensions:CGSizeMake(50, 50) // width, height 
                         alignment:CCTextAlignmentCenter
                          fontName:@"Helvetica-Bold" 
                          fontSize:24];
  di.color = ccc3(255, 255, 255);
  di.position = ccp(dicebg.position.x, dicebg.position.y-10);
  [ThisLayer addChild:di z:150];

  id rollact = [CCSequence actions:
          [CCCallFunc actionWithTarget:self selector:@selector(roll_dice)], 
          [CCDelayTime actionWithDuration:0.1f], 
          nil];
  onRoll = YES;
  [di runAction:[CCRepeatForever actionWithAction:rollact]];
  
}

-(void) roll_dice
{
  //di.string = [NSString stringWithFormat:@"%d",((rand()%6)+1)]; // 1..6 ?
  di.string = [NSString stringWithFormat:@"%d", 3]; // TEST
}

-(BOOL)isRoll
{
  return onRoll;
}

-(int) stopRoll
{
  if (onRoll == NO )
  {
    CCLOG(@"dice onRill == NO 인데 stopRoll 호출 됨");
    return -1;
  }
  NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
  [f setNumberStyle:NSNumberFormatterDecimalStyle];
  [di stopAllActions];
  onRoll = NO;
  return [[f numberFromString:di.string] intValue];
}

-(void) removeDice
{
  [di stopAllActions];
  id removeAnimation_anddie = [CCSequence actions:
                               [CCDelayTime actionWithDuration:0.5f], 
                               [CCFadeOut actionWithDuration:1.0f], 
                               [CCCallFuncN actionWithTarget:self selector:@selector(andDie:)],
                               nil];
  [dicebg runAction:removeAnimation_anddie];
  [di runAction:[removeAnimation_anddie copy]];  
}

-(void) andDie:(id)sender
{
  if (sender == dicebg)
  {
    [ThisLayer removeChild:dicebg cleanup:YES];
    //NSLog(@"die dicd bg");
    return;
  }
  if (sender == di)
  {
    [ThisLayer removeChild:di cleanup:YES];
    //NSLog(@"dir di");
    return;
  }
}

@end
