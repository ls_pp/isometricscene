//
//  isoDice.h
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 17..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface isoDice : NSObject
{
  CCSprite *dicebg;
  CCLabelTTF *di;
  CCLayer *ThisLayer;
  BOOL onRoll;
}

-(id) initWithLayer:(CCLayer *)layer;
-(void) drawDice;
-(BOOL) isRoll;
-(int) stopRoll;
-(void) removeDice;

-(void) drawNumberAndAction;
@end
