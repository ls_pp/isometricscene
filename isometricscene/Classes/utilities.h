//
//  utilities.h
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 17..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
@interface utilities : NSObject

@end
*/

// retina 인 경우는 ?
#define SCREEN_HEIGHT 320
#define SCREEN_WIDTH 480

#define CUBE_UNIT_WIDTH 64
#define CUBE_UNIT_HEIGHT 32


CGPoint _convert_map2ccp(int _xx, int _yy, int _zz);
