//
//  isoPlayer.h
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 17..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "isoCube.h"
#import "CCSequenceHelper.h"
#import "isoCards.h"

@interface isoPlayer : NSObject
{
  CCLayer *ThisLayer;
  CCSprite *sprite;
  isoCube * ThisCube; // 플레이어가 서 있는 큐브 
  int positionOnCube; // 큐브 위에서의 삐딱하게 서있는 정도 캐릭터 생성 할 때 마다 랜덤으로 +- 할지 ??
  
  int charDirection; // 방향 설정 0:아직 결정 안함 1: 왼쪽 2:오른쪽
  BOOL onAnimation; // turn과 관계 없이 애니메이션 중인지를 표시하는 플래그 
  // 턴 관련 플래그 
  int wholeCourseCount; // 몇바퀴 돌았는지 카운트 

  int score;  
  
  // 카드 관련 플래그 
  isoCards *myDeck;
}

// 초기화 
-(id) initWithStartingCube:(isoCube *)startCube onCCLayer:(CCLayer *)layer useTexture:(int)textureSet;

// sprite, position 관련 
-(CGPoint) getPosition;
// cube 관련
-(isoCube *) getStandingCube;

// 동작 관련 
@property (readonly) BOOL onAnimation;
-(void) rollMoves:(int)dice;
-(void) moveToNextBlock;
-(id) actionTo_xx:(int)_x yy:(int)_y;
-(void) moveTo_xx:(int)_xx yy:(int)_yy;
//-(void) moveTo_xx:(int)_xx yy:(int)_yy diceNum:(int)dice;

// score 관련
-(int) getPlayerScore;
-(void) setPlayerScore:(int)_score;

// 
@end