//
//  isoPlayer.m
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 17..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import "isoPlayer.h"
#import "utilities.h"

@implementation isoPlayer

@synthesize onAnimation;

//-(id) initWithStartingPosition_CCLayer:(CCLayer *)layer xx:(int)_xx yy:(int)_yy
-(id) initWithStartingCube:(isoCube *)startCube onCCLayer:(CCLayer *)layer useTexture:(int)textureSet
{
  ThisLayer = layer;
  ThisCube = startCube;
  onAnimation = NO;
  charDirection = 2; // 초기값으로 오른쪽 
  
  if (textureSet == 1) {
    sprite = [CCSprite spriteWithFile:@"char1.png"];
    positionOnCube = CUBE_UNIT_HEIGHT/2 + 10 ;
  } else if (textureSet == 2) {
    sprite = [CCSprite spriteWithFile:@"char2.png"];
    positionOnCube = CUBE_UNIT_HEIGHT/2 + 5 ;
  }
  sprite.position = ccpAdd(_convert_map2ccp(startCube.xx, startCube.yy, 0), ccp(0, positionOnCube));
  
  [ThisLayer addChild:sprite z:(100 - positionOnCube)];
  
  return self;
}

-(void) _startAnimation
{
  onAnimation = YES;
}

-(void) _stopAnimation
{
  onAnimation = NO;
}

-(void) rollMoves:(int)dice
{
  NSMutableArray *seqArray = [[NSMutableArray alloc] init];
  
  if (charDirection == 0) // 아직 방향을 정하지 않았다면 ? issue #2 
  {
    
  }
  
  [seqArray addObject:[CCCallFunc actionWithTarget:self selector:@selector(_startAnimation)]];
  for (int d=0; d<dice; d++) {
    [seqArray addObject:[CCCallFunc actionWithTarget:self selector:@selector(moveToNextBlock)]];
    [seqArray addObject:[CCDelayTime actionWithDuration:0.5f]];
  }
  [seqArray addObject:[CCCallFunc actionWithTarget:self selector:@selector(_stopAnimation)]];
  
  [sprite runAction:[CCSequenceHelper actionMutableArray:seqArray]];
}

-(void) moveToNextBlock//:(isoCube *)thisCube
{
  if (charDirection == 2) // right 
  {
    //CCLOG(@"right nextTo[%d][%d]", [ThisCube getRightCube].xx, [ThisCube getRightCube].yy);
    [self moveTo_xx:[ThisCube getRightCube].xx yy:[ThisCube getRightCube].yy];
    ThisCube = [ThisCube getRightCube];
  } else if (charDirection == 1) { // left 
    //CCLOG(@"left nextTo[%d][%d]", [ThisCube getLeftCube].xx, [ThisCube getLeftCube].yy);
    [self moveTo_xx:[ThisCube getLeftCube].xx yy:[ThisCube getLeftCube].yy];
    ThisCube = [ThisCube getLeftCube];
  }
}

-(isoCube *) getStandingCube
{
  return ThisCube;
}

// 지정한 곳으로 보내는 메소드 (출발지점으로 갈 때 필요) 
-(void) moveTo_xx:(int)_xx yy:(int)_yy
{
  //NSLog(@"player moveTo1 in");
  CGPoint targetPoint = ccpAdd(_convert_map2ccp(_xx, _yy, 0), ccp(0, positionOnCube));
  
  id ani = [CCSequence actions:
            [CCMoveTo actionWithDuration:0.3f position:targetPoint], 
            //[CCCallFunc actionWithTarget:self selector:@selector(___)]
            // 무브턴 끝 
            // 내땅인지 남의 땅인지
            // 남의땅이면 전투 시작 
            // 내 땅이면 업그레이드 크리쳐 번환 등 
            // 아무의 땅도 아니면 ... 
            nil];
  [sprite runAction:ani];
  //NSLog(@"player moveTo1 out");
}
// 시퀀스를 만들기 위해 액션으로 리턴 
-(id) actionTo_xx:(int)_x yy:(int)_y
{
  CGPoint targetPoint = ccpAdd(_convert_map2ccp(_x, _y, 0), ccp(0, positionOnCube));
  return [CCMoveTo actionWithDuration:0.3f position:targetPoint];
}

-(int) getPlayerScore
{
  return score;
}

-(void) setPlayerScore:(int)_score
{
  score = _score;
}

-(CGPoint) getPosition
{
  return sprite.position;
}

/*
-(void) moveTo_xx:(int)_xx yy:(int)_yy diceNum:(int)dice
{
  NSLog(@"player moveTo in");
  CGPoint targetPoint = ccpAdd(_convert_map2ccp(_xx, _yy, 0), ccp(0, CUBE_UNIT_HEIGHT/2));
  
  id ani = [CCSequence actions:
            [CCMoveTo actionWithDuration:(0.5f * dice) position:targetPoint], 
            //[CCCallFunc actionWithTarget:self selector:@selector(___)]
            // 무브턴 끝 
            // 내땅인지 남의 땅인지
            // 남의땅이면 전투 시작 
            // 내 땅이면 업그레이드 크리쳐 번환 등 
            // 아무의 땅도 아니면 ... 
            nil];
  [sprite runAction:ani];
  NSLog(@"player moveTo out");
}
*/
@end
