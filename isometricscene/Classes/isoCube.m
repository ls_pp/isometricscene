//
//  isoCube.m
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 17..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import "isoCube.h"
#import "utilities.h"

@implementation isoCube

@synthesize xx, yy; // zz ?

-(id)initWithCCLayer:(CCLayer *)layer onMap:(BOOL[40][40][20])_map atxx:(int)_xx atyy:(int)_yy atzz:(int)_zz drawSkeleton:(BOOL)doDraw
{
  ThisLayer = layer;
  
  SkeletonOpacity = 75;
  
  xx = _xx;
  yy = _yy;
  zz = _zz;
  
  drawGuide1 = _map[xx-1][yy][zz];
  drawGuide2 = _map[xx][yy+1][zz];
  
  leftCubes = [[NSMutableArray alloc] init];
  rightCubes = [[NSMutableArray alloc] init];
  
  ownPlayerNumber = -1; // 초기화에선 누구의 소유도 아님 
  price = 0;
  level = 1;
  
  if (doDraw == YES)
  {
    [self drawCube];
  }
  
  return self;
}

-(int) getPrice
{
  return price;
}

-(void) setOwner:(int)playerNumber // object?:()
{
  ownPlayerNumber = playerNumber;
  // 크리쳐 종류나 그런것도 여기에 .. 
  creatureSprite = [CCSprite spriteWithFile:@"object.png"];
  // 위에서 쿵 하고 떨어지는 에니메이션도 좋겠다 속성별로 불은 화염이 일고 바람은 먼지 풀풀 날리면서 
  creatureSprite.anchorPoint = CGPointMake(0.5, 0); // 아래쪽bottom 가운데 center
  creatureSprite.position = ccpAdd(cb_top.position, ccp(0, 100));
  [ThisLayer addChild:creatureSprite z:11];
  [creatureSprite runAction:[CCMoveBy actionWithDuration:0.3f position:ccp(0, -100)]];
  //[creature addObject:creatureSprite];
  // 
  price = 16 * level;
  pricelbl = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", price]
                                       dimensions:CGSizeMake(50, 15) // width, height 
                                        alignment:CCTextAlignmentCenter
                                         fontName:@"Helvetica" 
                                         fontSize:11];
  pricelbl.color = ccc3(0, 0, 0); //black
  pricelbl.position = ccpAdd(cb_top.position, ccp(0, -10)); 
  [ThisLayer addChild:pricelbl z:72];

  
}

-(int) getOwnPlayerNumber
{
  // 어디서는 1번 플레이어 2번 플레이어 이렇게 쓰이는데
  // 여기서는 0, 1 이런식으로 인덱스로 쓰이니 혼동의 가능성이 있음 
  return ownPlayerNumber;
}

-(BOOL) isOwned
{
  if (ownPlayerNumber == -1) return NO;
  return YES;
}

-(void) addLeftCube:(isoCube *)lcube {
  if (lcube == nil) 
  {
    CCLOG(@"addLeftCube nil will not added leftcubes");
    return; 
  }
  [leftCubes addObject:lcube];
}
-(void) addRightCube:(isoCube *)rcube {
  if (rcube == nil) 
  {
    CCLOG(@"addRightCube nil will not added leftcubes");
    return; 
  }
  [rightCubes addObject:rcube];
}
-(isoCube *) getRightCube {
  if ([rightCubes count] == 0) return nil;
  return [rightCubes objectAtIndex:0];
}
-(isoCube *) getLeftCube {
  if ([leftCubes count] == 0) return nil;
  return [leftCubes objectAtIndex:0];  
}

-(void) drawCube
{
  CGPoint point = _convert_map2ccp(xx, yy, zz);
  
  [self drawCB_top:point];
  
  // z 가 끼어들면 복잡해 지겠지만 일단 나중에 
  //NSLog(@"_map[%d][%d][%d]:%d", _xx+1,_yy,_zz,_map[_xx+1][_yy][_zz]);
  //NSLog(@"_map[%d][%d][%d]:%d", _xx,_yy+1,_zz,_map[_xx][_yy+1][_zz]);
  
  if (xx == 0 || (xx-1 >=0 && drawGuide1 == NO))
  {
    [self drawCB_left:point]; 
    hasLeftSide = YES;
  }
  
  if (drawGuide2 == NO)
  {
    [self drawCB_right:point];            
    hasRightSide = YES;
  }  
}

// 이 블록이 놓임으로써 뒤 블록의 객체는 가려지게 될테니 그것을 정리 
-(void) cleanupWalls:(isoCube *)sideCube leftSide:(BOOL)left {
  if (left==YES) // 오른쪽 벽 : xx증감 
  {
    [sideCube remove_leftSide];
    //NSLog(@"erase left of [%d][%d]", sideCube.xx, sideCube.yy); 
  } else {
    [sideCube remove_rightSide];
    //NSLog(@"erase right of [%d][%d]", sideCube.xx, sideCube.yy); 
  }
}

-(CGPoint) getPosition
{
  return cb_top.position;
}

-(void) setCubeType:(int)ctype {
  cubeType = ctype;
}
-(int) getCubeType {
  return cubeType;
}

-(void) drawCB_top:(CGPoint)point { 
  //CCSprite *cb_top;
  if (cubeType == 0)
  {
    cb_top = [CCSprite spriteWithFile:@"cube_texture_top.png"];
    cb_top.opacity = SkeletonOpacity;  
  } else if (cubeType == 1) { // 불 
    cb_top = [CCSprite spriteWithFile:@"cube_fire_64*32.png"];
  } else if (cubeType == 2) { // 바람  
    cb_top = [CCSprite spriteWithFile:@"cube_wind_64*32.png"];
  } else if (cubeType == 3) { // 땅
    cb_top = [CCSprite spriteWithFile:@"cube_earth_64*32.png"];
  } else if (cubeType == 4) { // 물  
    cb_top = [CCSprite spriteWithFile:@"cube_water_64*32.png"];
  } else if (cubeType == 5) { // 무속성  
    cb_top = [CCSprite spriteWithFile:@"cube_none_64*32.png"];
  }
  cb_top.position = point; 
  [ThisLayer addChild:cb_top z:10];
}

-(void) drawCB_right:(CGPoint)point {
  cb_right = [CCSprite spriteWithFile:@"cube_texture_side32*32.png"];
  cb_right.flipY = YES;
  cb_right.position = ccp(point.x +16, point.y - 16);
  cb_right.opacity = SkeletonOpacity;
  [ThisLayer addChild:cb_right z:9];    
}
-(void) drawCB_left:(CGPoint)point {
  cb_left = [CCSprite spriteWithFile:@"cube_texture_side32*32.png"];
  cb_left.position = ccp(point.x - 16, point.y - 16);
  cb_left.opacity = SkeletonOpacity;
  [ThisLayer addChild:cb_left z:9];
}
-(void) remove_leftSide {
  if (hasLeftSide == YES) [ThisLayer removeChild:cb_left cleanup:YES];
  hasLeftSide = NO;
}
-(void) remove_rightSide {
  if (hasRightSide == YES) [ThisLayer removeChild:cb_right cleanup:YES];
  hasRightSide = NO;
}

@end
