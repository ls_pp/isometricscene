//
//  isoCube.h
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 17..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface isoCube : NSObject
{
  CCLayer *ThisLayer;
  
  CCSprite *cb_top, *cb_left;
  CCSprite *cb_right;
  
  BOOL hasLeftSide, hasRightSide;
  BOOL drawGuide1, drawGuide2;
  
  int xx, yy, zz;
  
  int SkeletonOpacity; // 텍스쳐가 없을 때 투명도 
  
  // 속성 1:불, 2:바람, 3:땅, 4:물, 5:무
  //     10castle/11north/12south
  //     20
  int cubeType; 
  
  // 깔려 있는 크리쳐는 ?
  CCSprite *creatureSprite;
  CCLabelTTF *pricelbl;
  
  // 크리쳐의 주인은 누구? -1(주인없음), 0,1,2,3,4 플레이어 
  int ownPlayerNumber; 
  NSMutableArray *creature; // 크리쳐, 건물을 통칭해서 뭐라고 부르지 .. 
  // 
  
  // 가격   
  // 호부, 크리쳐보너스
  // '' 주변의 영향은 어떻게 계산하지? 
  int price; 
  int level;    // 업그레이드 레벨 
  // 크리처가 놓일때, 업그레이드 할 때 등등 이벤트때 재계산을 호출 
  
  // next cube에 해당하는 객체의 포인터 보관. 포인터인지 좌표인지 아직 모르겠음 
  NSMutableArray *leftCubes;
  NSMutableArray *rightCubes;
}

// 초기화 
-(id)initWithCCLayer:(CCLayer *)layer onMap:(BOOL[40][40][20])_map atxx:(int)_xx atyy:(int)_yy atzz:(int)_zz drawSkeleton:(BOOL)doDraw;

// 위치, 속성 관련 
@property (readonly) int xx;
@property (readonly) int yy;
//@property (readonly) int zz;

-(CGPoint) getPosition;
-(int) getCubeType;
-(isoCube *) getRightCube;
-(isoCube *) getLeftCube;
-(void) addLeftCube:(isoCube *)lcube;
-(void) addRightCube:(isoCube *)rcube;
-(void) setCubeType:(int)ctype;

// 소유 관련 
-(BOOL) isOwned;
-(int) getOwnPlayerNumber;
-(void) setOwner:(int)playerNumber;
-(int) getPrice;

// 화면 표시 관련 
-(void) drawCube;
-(void) cleanupWalls:(isoCube *)sideCube leftSide:(BOOL)left;
-(void) drawCB_top:(CGPoint)point;
-(void) drawCB_left:(CGPoint)point;
-(void) drawCB_right:(CGPoint)point;
-(void) remove_leftSide;
-(void) remove_rightSide;

@end
