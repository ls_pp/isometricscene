//
//  isoGame.m
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 16..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import "isoGame.h"

@implementation isoGame

-(id) initWithCCLayer:(CCLayer *)thisLayer standardZ:(int)zidx
{
  ThisLayer = thisLayer;
  
  CGSize screenSize = [[CCDirector sharedDirector] winSize];
  SCREEN_HEIGHT = screenSize.height;
  SCREEN_WIDTH = screenSize.width;
  
  CUBE_UNIT_WIDTH = 64;
  CUBE_UNIT_HEIGHT = 32; 
 
  cubes = [[NSMutableArray alloc] init];
  players = [[NSMutableArray alloc] init];
  ownedCubes = [[NSMutableArray alloc] init];
  _PlayerNumber = 0;
  GAMETURNS = 1;

  return self;
}
//

// turn 관리 
-(void) addTurnCount {
  GAMETURNS++;
  //CCLOG(@"now Turn Count: %d", GAMETURNS);
}
-(int) getTurnCount {
  return GAMETURNS;
}
//

//player
-(void) setPlayerNumber:(int)num init:(BOOL)init
{
  _PlayerNumber = num;
  
  if (init == YES)
  {
    for (int p=1; p<=num; p++) {
      //isoPlayer *pp = [[isoPlayer alloc] initWithStartingPosition_CCLayer:ThisLayer xx:startCube.xx yy:startCube.yy];
      // 사용 할 텍스쳐를 여기서 넘겨 줄 수 있을까?
      isoPlayer *pp = [[isoPlayer alloc] initWithStartingCube:startCube onCCLayer:ThisLayer useTexture:p];
      [players addObject:pp];
      [ownedCubes addObject:[[NSMutableArray alloc] init]];
    }
  }
  //CCLOG(@"ownedCubes count:%d", [ownedCubes count]);
}

-(NSMutableArray *) getPlayers {
  return players;
}

-(void) movePlayer_no:(int)pn dice:(int)dice
{
  // 정확하게는 현재의 위치 + dice로 갈 수 있는 곳 
  //CCLOG(@"movePlayer: dice=%d", dice);
  isoPlayer *player = [players objectAtIndex:pn];
  [player rollMoves:dice];
}
//

// dice
-(BOOL) isRollDice {
  return [_dice isRoll];
}
-(void) drawDice {
  _dice = [[isoDice alloc] initWithLayer:ThisLayer];
  [_dice drawDice];
}
-(int) stopDice {
  return [_dice stopRoll];
}
-(void) removeDice {
  [_dice removeDice];
}
//

// map and cubes
-(void) reportBrokenLinks
{
  BOOL broken = NO;
  for (isoCube *c in cubes)
  {
    if ([c getLeftCube] == nil) 
    {
      CCLOG(@"reportBrokenLinks cubes[%d][%d] has not LeftCube", c.xx, c.yy);
      broken = YES;
    }
    if ([c getRightCube] == nil) 
    {
      CCLOG(@"reportBrokenLinks cubes[%d][%d] has not RightCube", c.xx, c.yy);
      broken = YES;
    }
  }
  if (broken == NO) CCLOG(@"reportBrokenLinks no broken links");
}

-(void) bondCubesLeft:(isoCube *)lCube Right:(isoCube *)rCube {
  // lcube의 오른쪽에 rcube를 넣고 
  [lCube addRightCube:rCube];
  // rcube의 왼쪽에 lcube를 넣으면 bonding 
  [rCube addLeftCube:lCube];
}

-(isoCube *) getFirstCube {
  return [cubes objectAtIndex:0];
}
-(isoCube *) getLastCube {
  if ([cubes count] > 0)
  {
    return [cubes objectAtIndex:([cubes count]-1)];
  } else {
    NSLog(@"getLastCube but [cubes count] == 0 return nil");
    return nil;
  }
}
-(void) setStartingCube:(isoCube *)_startCube {
  startCube = _startCube;
  //#12 성 이미지 
  CCSprite *castle = [CCSprite spriteWithFile:@"castle60x68.png"];
  castle.position = ccpAdd([startCube getPosition], ccp(0, [castle boundingBox].size.height/2));
  [ThisLayer addChild:castle z:50];
}

-(void) setCube:(isoCube *)cube owner:(int)playerNumber //creature:(NSMutableArray *)creature
{
  CCLOG(@"[cube setOwner:%d]",playerNumber);
  [cube setOwner:playerNumber];
  isoPlayer *p = [players objectAtIndex:playerNumber];
  //CCLOG(@"before %d", [p getPlayerScore]);
  [p setPlayerScore:([p getPlayerScore] - 50)];
  //CCLOG(@"after %d", [p getPlayerScore]);
  // 점수나 그런거 계산 우르르 .. 여기서? 게임 구현에서 ?
  [[ownedCubes objectAtIndex:playerNumber] addObject:cube];
  CCLOG(@"p#:%d has %d lands", playerNumber, [[ownedCubes objectAtIndex:playerNumber] count]);
}

-(void) collectBill:(int)playerNumber
{
  CCLOG(@"collectBill:%d", playerNumber);
  isoPlayer *p = [players objectAtIndex:playerNumber];
  int cost = [[p getStandingCube] getPrice];
  isoPlayer *owner = [players objectAtIndex:[[p getStandingCube] getOwnPlayerNumber]];
  [p setPlayerScore:[p getPlayerScore]-cost];
  [owner setPlayerScore:[owner getPlayerScore]-cost];
  
  CCLabelTTF *pbill = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"-%d", cost]
                                         dimensions:CGSizeMake(50, 15) // width, height 
                                          alignment:CCTextAlignmentCenter
                                           fontName:@"Helvetica" 
                                           fontSize:11];
  pbill.color = ccc3(255, 0, 0); //red
  pbill.position = ccpAdd([p getPosition], ccp(0, 30)); 
  [ThisLayer addChild:pbill z:301];

  id minusAction = [CCMoveBy actionWithDuration:1.5f position:ccp(0, 30)];
  [pbill runAction:[CCSequence actions:minusAction, 
                    [CCCallFuncN actionWithTarget:self selector:@selector(removeObjectInThisLayer:)], 
                    nil]];
  
  CCLabelTTF *obill = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"+%d", cost]
                                       dimensions:CGSizeMake(50, 15) // width, height 
                                        alignment:CCTextAlignmentCenter
                                         fontName:@"Helvetica" 
                                         fontSize:11];
  obill.color = ccc3(0, 0, 255); //blue
  obill.position = ccpAdd([owner getPosition], ccp(0, 60)); 
  [ThisLayer addChild:obill z:301];

  id plusAction = [CCMoveBy actionWithDuration:1.5f position:ccp(0, -30)];
  [obill runAction:[CCSequence actions:plusAction, 
                    [CCCallFuncN actionWithTarget:self selector:@selector(removeObjectInThisLayer:)], 
                    nil]];
}
-(void) removeObjectInThisLayer:(id)sender
{
  [ThisLayer removeChild:sender cleanup:YES];
}

// 수동 맵 생성 
-(void) putCubeOnMap_xx:(int)xx mapyy:(int)yy zz:(int)zz cubeType:(int)ctype
{
  //CCLOG(@"putCubeOn[%d][%d]", xx, yy);
  // 놓을 자리 청소를 먼저 하고 
  if ([cubes count] > 0) 
  { 
    // count-1 즉 바로 전의 블록이라는 보장이 있나?
    for (isoCube *cc in cubes) 
    {
      if ((cc.xx == xx+1 && cc.yy == yy))// || (xx-1>0 && cc.xx == xx-1 && cc.yy == yy))
      {
        [cc cleanupWalls:cc leftSide:YES]; // xx 증감이니까 왼쪽 벽 
      } else if (//(cc.xx == xx && cc.yy == yy+1) || 
                 (cc.xx == xx && cc.yy == yy-1)) {
        [cc cleanupWalls:cc leftSide:NO]; // yy 증감이니까 오른쪽 벽         
      }
    }

  }
  // 그린다
  isoCube *c = [[isoCube alloc] initWithCCLayer:ThisLayer onMap:_map atxx:xx atyy:yy atzz:zz drawSkeleton:NO];
  [c setCubeType:ctype];
  [c addLeftCube:[self getLastCube]]; // 이전 블록이 지금 블록의 왼쪽 이고 
  [[self getLastCube] addRightCube:c]; // 이전 블록의 오른쪽 블록이 지금 블록
  [c drawCube];
  
  [cubes addObject:c];
  //NSLog(@"cubes count:%d", [cubes count]);
  _mapZ[xx][yy] = zz;
  _map[xx][yy][zz] = YES;
  //NSLog(@"_mapZ[%d][%d]:%d", xx, yy, _mapZ[xx][yy]);
}

@end
