//
//  HelloWorldLayer.m
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 16..
//  Copyright noizze.net 2012년. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "isoPlayer.h"

// HelloWorldLayer implementation
@implementation HelloWorldLayer

+(CCScene *) scene {
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
    self.isTouchEnabled = YES;
    
    CCSprite *bg = [CCSprite spriteWithFile:@"land_bg.jpg"];
    bg.position = ccp(240, 160);
    [self addChild:bg z:0 tag:100];
    
    [self runAction:[CCSequence actions:
                     [CCDelayTime actionWithDuration:0.5f], 
                     [CCCallFunc actionWithTarget:self selector:@selector(drawObjects)],
                     [CCCallFunc actionWithTarget:self selector:@selector(drawScoreBoard)],
                     nil]];
	}
	return self;
}

-(void) drawObjects
{
  Game = [[isoGame alloc] initWithCCLayer:self standardZ:0]; // with what ?
  // stage class ? = isoGame 클래스 아닐까 
  //Game = [[isoGame alloc] initWithCCLayer:self map:(isoMapStructre)map textures:...]; // 이렇게 초기화를 하던가 
  // [Game importTextures:(NSMutableDictionary *) indexname as key, pointer as value
  // [Game addMapStructure:map];
  // [Game drawBackground];
  // 또는 
  // [Game addCube: keyValue 형태로 
  //                  xx, yy, texture,
  //                  속성 : 물/불/바람/자연/무, 특수:시작, 터닝포인트
  //                등등을 가진 dictionary 를 add? ]
  // 다 하고, [Game startStage] 로 화면에 그리고 플레이어 배치 준비 

  // 큐브 속성 1:불, 2:바람, 3:땅, 4:물, 5:무
  [Game putCubeOnMap_xx:4 mapyy:0 zz:1 cubeType:5]; // castle 
  [Game setStartingCube:[Game getLastCube]];
  // 땅=자연=나무 속성 
  [Game putCubeOnMap_xx:4 mapyy:1 zz:1 cubeType:3]; 
  [Game putCubeOnMap_xx:4 mapyy:2 zz:1 cubeType:3]; 
  [Game putCubeOnMap_xx:4 mapyy:3 zz:1 cubeType:3]; 
  [Game putCubeOnMap_xx:4 mapyy:4 zz:1 cubeType:3]; 
  // 바람 
  [Game putCubeOnMap_xx:4 mapyy:5 zz:1 cubeType:2]; 
  [Game putCubeOnMap_xx:3 mapyy:5 zz:1 cubeType:2]; 
  [Game putCubeOnMap_xx:2 mapyy:5 zz:1 cubeType:2]; 
  [Game putCubeOnMap_xx:1 mapyy:5 zz:1 cubeType:2]; 
  // south
  [Game putCubeOnMap_xx:0 mapyy:5 zz:1 cubeType:5]; 
  // 불
  [Game putCubeOnMap_xx:0 mapyy:4 zz:1 cubeType:1]; 
  [Game putCubeOnMap_xx:0 mapyy:3 zz:1 cubeType:1]; 
  [Game putCubeOnMap_xx:0 mapyy:2 zz:1 cubeType:1]; 
  [Game putCubeOnMap_xx:0 mapyy:1 zz:1 cubeType:1]; 
  // 물
  [Game putCubeOnMap_xx:0 mapyy:0 zz:1 cubeType:4]; 
  [Game putCubeOnMap_xx:1 mapyy:0 zz:1 cubeType:4]; 
  [Game putCubeOnMap_xx:2 mapyy:0 zz:1 cubeType:4]; 
  [Game putCubeOnMap_xx:3 mapyy:0 zz:1 cubeType:4]; 
  // 스테이지1 구성 끝 
 // [Game reportBrokenLinks];
  [Game bondCubesLeft:[Game getLastCube] Right:[Game getFirstCube]];
  //CCLOG(@"test again");
  //[Game reportBrokenLinks];
  // 
  [Game setPlayerNumber:MAX_PLAYERS init:YES]; 
  [[[Game getPlayers] objectAtIndex:0] setPlayerScore:200]; // 컬셉처럼 200으로 시작 
  [[[Game getPlayers] objectAtIndex:1] setPlayerScore:200]; // 컬셉처럼 200으로 시작 
  
  //CCLOG(@"player#%d", [Game getPlayerNumber]);
  // 플레이어 턴은 이벤트 관련이니 여기서 제어 하는게 낫겠다. 
  
  
  // 게임 시작 
  nowPlayerNumber = 0;
  nowPlayerTurn = my_turn; 
  [self stateController];
}

-(void) drawScoreBoard
{
  CCSprite *sc_bg;
  CCSprite *p1;
  CCSprite *p2;
  
  // 기존에 있으면 패스 #16 
  if ([self getChildByTag:90] == nil)
  {
    sc_bg = [CCSprite spriteWithFile:@"score_bg.png"];
    // size 100*65
    sc_bg.position = ccp(480-50-5, 320-65/2-5);
    sc_bg.opacity = 200;
    [self addChild:sc_bg z:300 tag:90];
    
    p1 = [CCSprite spriteWithFile:@"p1icon_15x15.png"];
    p2 = [CCSprite spriteWithFile:@"p2icon_15x15.png"];
    p1.position = ccp(sc_bg.position.x - 40, sc_bg.position.y +15);
    p2.position = ccp(p1.position.x, p1.position.y - 20);
    [self addChild:p1 z:301 tag:91];
    [self addChild:p2 z:301 tag:92];
  } else {
    [self removeChild:[self getChildByTag:93] cleanup:YES]; // p1 score
    [self removeChild:[self getChildByTag:94] cleanup:YES]; // p2 score 
    [self removeChild:[self getChildByTag:95] cleanup:YES]; // stage number 
    
    sc_bg = (CCSprite *)[self getChildByTag:90];
    p1 = (CCSprite *)[self getChildByTag:91];
    p2 = (CCSprite *)[self getChildByTag:92];
  }
  
  // 다시그리기 #16
  CCLabelTTF *p1score = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", [[[Game getPlayers] objectAtIndex:0] getPlayerScore]]
                                         dimensions:CGSizeMake(50, 15) // width, height 
                                          alignment:CCTextAlignmentLeft
                                           fontName:@"Helvetica" 
                                           fontSize:11];
  p1score.anchorPoint = CGPointMake(0, 0.5);
  p1score.color = ccc3(0, 0, 0); //black
  p1score.position = ccp(p1.position.x + 15, p1.position.y); // anchor를 조정 할 필요도 있음 
  [self addChild:p1score z:301 tag:93];
  
  CCLabelTTF *p2score = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", [[[Game getPlayers] objectAtIndex:1] getPlayerScore]]
                                         dimensions:CGSizeMake(50, 15) // width, height 
                                          alignment:CCTextAlignmentLeft
                                           fontName:@"Helvetica" 
                                           fontSize:11];
  p2score.anchorPoint = CGPointMake(0, 0.5);
  p2score.color = ccc3(0, 0, 0); //black
  p2score.position = ccp(p2.position.x + 15, p2.position.y); // anchor를 조정 할 필요도 있음 
  [self addChild:p2score z:301 tag:94];
  
  CCLabelTTF *turnNum = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"T#:%d", [Game getTurnCount]]
                                         dimensions:CGSizeMake(50, 15) // width, height 
                                          alignment:CCTextAlignmentLeft
                                           fontName:@"Helvetica" 
                                           fontSize:12];
  turnNum.anchorPoint = CGPointMake(0, 0.5);
  turnNum.color = ccc3(0, 0, 0); //black
  turnNum.position = ccp(sc_bg.position.x + 20, sc_bg.position.y + 20);
  [self addChild:turnNum z:301 tag:95];

}

-(void) showStartTurnBoard {
  CCSprite *bg = [CCSprite spriteWithFile:@"score_bg.png"];
  bg.position = ccp(240, 160); // center?
  //bg.opacity = 128;
  bg.scale = 0.1f;
  [self addChild:bg z:302 tag:60];
  [bg runAction:[CCSequence actions:
                 //[CCFadeIn actionWithDuration:0.5f],
                 [CCScaleTo actionWithDuration:0.3f scale:1.0f],
                 [CCCallFunc actionWithTarget:self selector:@selector(_startTurnBoard_sub)],
                 nil]];  
}
-(void) _startTurnBoard_sub {
  CCNode *bg = [self getChildByTag:60];
  CCLabelTTF *turnNum = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Turn #:%d", [Game getTurnCount]]
                                         dimensions:CGSizeMake(100, 15) // width, height 
                                          alignment:CCTextAlignmentCenter
                                           fontName:@"Helvetica" 
                                           fontSize:13];
  //turnNum.anchorPoint = CGPointMake(0, 0.5);
  turnNum.color = ccc3(0, 0, 0); //black
  turnNum.position = ccp(bg.position.x, bg.position.y + 20);
  [self addChild:turnNum z:303 tag:61];
  
  // player%d's phase.
  CCLabelTTF *pl = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Player%d's Phase.", nowPlayerNumber]
                                    dimensions:CGSizeMake(110, 15) // width, height 
                                     alignment:CCTextAlignmentCenter
                                      fontName:@"Helvetica" 
                                      fontSize:13];
  pl.color = ccc3(0, 0, 0);
  pl.position = ccp(turnNum.position.x, turnNum.position.y - 30);
  [self addChild:pl z:303 tag:62];
}

/**
 */
-(void) stateController
{
  CCLOG(@"stateController nowPlayerNumber:%d/turn:%d", nowPlayerNumber, nowPlayerTurn);

  switch (nowPlayerTurn) {
    case my_turn:      // 턴 확인 창이 팝 
      [self showStartTurnBoard]; 
      break;
      
    case look_infomations: // 카드를 뽑고 정리하는 메뉴 팝 (마일스톤2 에서는 이거 패스)
      nowPlayerTurn++;        // test
      [self stateController]; // test 재귀호출이 잘 되나?
      break;
      
    case roll_dice:
      [Game drawDice];
      break;
      
    case on_cube_command: //  땅관련 메뉴 팝  
      // 땅의 소유가 있는지 확인:"플레이어의 현재 포지션"에 해당하는 큐브"가  소유 상태인가?
      if ([[[[Game getPlayers] objectAtIndex:nowPlayerNumber] getStandingCube] isOwned] == NO)
      {
        // 돈이 되나? 
        if ([[[Game getPlayers] objectAtIndex:nowPlayerNumber] getPlayerScore] >= 50)
        {
        // 소유 할 것인가 질문 
        //isoCube *c= [[[Game getPlayers] objectAtIndex:nowPlayerNumber] getStandingCube];
        //CCLOG(@"buy[%d][%d]?", c.xx, c.yy);
        UIAlertView *view=[[UIAlertView alloc] initWithTitle:@"want to buy ?"
                                                     message:@"cost: 50"
                                                    delegate:self 
                                           cancelButtonTitle:@"NO"
                                           otherButtonTitles:@"YES", nil];
        [view setTag:997];
        [view show];
        [view release];
        } 
      } else {
        if ([[[[Game getPlayers] objectAtIndex:nowPlayerNumber] getStandingCube] getOwnPlayerNumber] == nowPlayerNumber)
        {
          CCLOG(@"여기는 내 땅");
          // 메뉴 팝 
          // 크리쳐 소환(교체)
          // 영지 : 레벨업, 속성변화
          // 패스
        } else {
          CCLOG(@"남의 땅");
          [Game collectBill:nowPlayerNumber];
          [self  drawScoreBoard];
          // 여기도 에니메이션이 들어가지 않으면 그냥 넘어가게 될 수도 ?
          [self nextPlayerTurn]; // 내부 턴을 1단계 전진 
        }
      }      
      break;
      
    case finish_turn:
      nowPlayerTurn = my_turn;
      [self passToNextPlayer];      
      break;
  }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  //CCLOG(@"%@/tag:%d/%d", alertView, alertView.tag, buttonIndex);
  // <UIAlertView: 0x8b96650; frame = (98 90; 284 141); opaque = NO; layer = <CALayer: 0x8b4e940>>:1
  if (alertView.tag == 997) // 20원 주고 사겠습니까?
  {
    if (buttonIndex == 1)    // 예 - 사겠음 
    {
      [Game setCube:[[[Game getPlayers] objectAtIndex:nowPlayerNumber] getStandingCube] owner:nowPlayerNumber];
      [self  drawScoreBoard];
    }
    [self nextPlayerTurn]; // 내부 턴을 1단계 전진 
  }
}

-(void) passToNextPlayer {
  id waitaction = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                     [CCDelayTime actionWithDuration:0.5f], 
                                                     [CCCallFunc actionWithTarget:self selector:@selector(passToNextPlayer_sub1)],
                                                     nil]
                   ];
  [waitaction setTag:999];
  [self runAction:waitaction];
}
-(void) passToNextPlayer_sub1 {
  isoPlayer *p = [[Game getPlayers] objectAtIndex:nowPlayerNumber];
  
  if (p.onAnimation == NO)
  {
    [self stopActionByTag:999]; // stop waitaction
    nowPlayerNumber++;
    if (nowPlayerNumber >= MAX_PLAYERS) 
    {
      [Game addTurnCount];
      nowPlayerNumber = 0; // reset  
    }  
    [self stateController];
  } else {
    CCLOG(@"이젠 절대 여기 올 일이 없을것임 앞단계에서 다 체크 했으니");
  }
}

-(int) nextPlayer
{
  nowPlayerNumber++;
  if (nowPlayerNumber > MAX_PLAYERS) 
  {
    [Game addTurnCount];
    nowPlayerNumber = 0; // reset 
  }
  
  return nowPlayerNumber;
}

-(void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
  NSArray* allTouches = [[event allTouches] allObjects];
  
  if ([allTouches count] == 1) 
  {
    CCLOG(@"ccTouchesEnded nowPlayerNumber:%d/tuen:%d", nowPlayerNumber, nowPlayerTurn);

    switch (nowPlayerTurn) {
      case my_turn:
        // StartTurnBoard 지우고 
        [self removeChild:[self getChildByTag:61] cleanup:YES];
        [self removeChild:[self getChildByTag:62] cleanup:YES];
        [self removeChild:[self getChildByTag:60] cleanup:YES];
        // 다음 차례 
        nowPlayerTurn++;
        [self stateController];
        break;
        
      case look_infomations:
        // 각 메뉴 별 터치 제어 
        break;
        
      case roll_dice: // nowPlayerTurn:2 
        [Game movePlayer_no:nowPlayerNumber dice:[Game stopDice]];      
        [Game removeDice];
        [self waitForPlayerMoving]; 
        break;
        
      case on_cube_command:
        // 여기도 메뉴가 뜨기 때문에 여러 단계가 있음 
        // 
        break;
      case finish_turn:
        break;
    }
  }
}

-(void) waitForPlayerMoving
{
  id waitaction = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                     [CCDelayTime actionWithDuration:0.5f], 
                                                     [CCCallFunc actionWithTarget:self selector:@selector(nextPlayerTurn)],
                                                     nil]
                   ];
  [waitaction setTag:998];
  [self runAction:waitaction];

}
  
-(void) nextPlayerTurn {
  isoPlayer *p = [[Game getPlayers] objectAtIndex:nowPlayerNumber];

  if (p.onAnimation == NO) // movePlayer_no 가 끝나면 ... 
  {
    [self stopActionByTag:998]; // stop waitForPlayerMoving    
    nowPlayerTurn++; // playerNumber가 아님.. 내부 단계를 다음으로 진행  
    [self stateController];
  } else{
    //CCLOG(@"onAnimation == YES");
  }
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
