//
//  HelloWorldLayer.h
//  isometricscene
//
//  Created by Choe Yong-uk on 12. 2. 16..
//  Copyright noizze.net 2012년. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "isoGame.h"

// HelloWorldLayer

typedef enum {
  my_turn          = 0, // 자기 순서임을 확인
  look_infomations = 1, // 정보 확인
  roll_dice        = 2, // 다이스 이동
  on_cube_command  = 3, //크리쳐 소환/영지 커맨드/패스
  finish_turn      = 4  //턴 종료
} _turn;

@interface HelloWorldLayer : CCLayer
{
  isoGame *Game;
  CGPoint *diffCamera;
  
  int nowPlayerNumber;
  _turn nowPlayerTurn;
  
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

-(void) drawObjects;
-(void) stateController;
-(void) showStartTurnBoard;
-(void) passToNextPlayer;
-(void) passToNextPlayer_sub1;
-(void) waitForPlayerMoving;
-(void) nextPlayerTurn;

@end

#define MAX_PLAYERS 2
